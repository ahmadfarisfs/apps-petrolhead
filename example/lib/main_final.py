from flask import Flask, request,jsonify
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
api = Api(app)
offset_location= 0.0
delivery_status = "IDLE"
lock_command = False
position_lat = 0.0
position_lon = 0.0
vibration = False
lock_status = True
volume = 0.0
theft = False

data = {
  "list": [
    {
      "DriverID": 1,
      "Name": "Ujang",
      "CarID": "W5768AB",
      "Contact" : "08123456789",
      "Source": "JKT",
      "Dest": "BDG",
      "Pos" : [-6.2528, 106.289],
      "FuelType": "Pertalite",
      "FuelVolume" : 16000,
      "CurrVolume" : 1000,
      "Warning" : "none",
      "Lateness" : 2,
      "Completed" : 0
    },
    {
      "DriverID": 2,
      "Name": "Deden",
      "CarID": "W5768AB",
      "Contact" : "08123456789",
      "Source": "JKT",
      "Dest": "BDG",
      "Pos" : [-6.2428, 106.399],
      "Route": "JKT - BDG",
      "FuelType": "Pertalite",
      "FuelVolume" : 1000,
      "CurrVolume" : 1000,
      "Warning" : "inactive",
      "Lateness" : 0,
      "Completed" : 0
    },
    {
      "DriverID": 3,
      "Name": "Lanceukna Deden",
      "CarID": "W5768AB",
      "Contact" : "08123456789",
      "Source": "JKT",
      "Dest": "BDG",
      "Pos" : [-6.2328, 106.499],
      "FuelType": "Pertalite",
      "FuelVolume" : 1000,
      "CurrVolume" : 1000,
      "Warning" : "none",
      "Lateness" : 3,
      "Completed" : 0
    },
    {
      "DriverID": 4,
      "Name": "Tati",
      "CarID": "W5768AB",
      "Contact" : "08123456789",
      "Source": "JKT",
      "Dest": "BDG",
      "Pos" : [-6.3828, 106.289],
      "FuelType": "Pertalite",
      "FuelVolume" : 1000,
      "CurrVolume" : 990,
      "Warning" : "theft",
      "Lateness" : 0,
      "Completed" : 1
    },
    {
      "DriverID": 5,
      "Name": "Jono",
      "CarID": "W5768AB",
      "Contact" : "08123456789",
      "Source": "JKT",
      "Dest": "BDG",
      "Pos" : [-6.3828, 106.379],
      "FuelType": "Pertalite",
      "FuelVolume" : 1000,
      "CurrVolume" : 1000,
      "Warning" : "none",
      "Lateness" : 0,
      "Completed" : 1
    },
    {
      "DriverID": 6,
      "Name": "Sembarang",
      "CarID": "W5768AB",
      "Contact" : "08123456789",
      "Source": "JKT",
      "Dest": "BDG",
      "Pos" : [-6.3728, 106.479],
      "FuelType": "Pertalite",
      "FuelVolume" : 1000,
      "CurrVolume" : 1000,
      "Warning" : "inactive",
      "Lateness" : 0,
      "Completed" : 0
    }
  ]
}

class FrontEndpoint(Resource):
    @cross_origin()
    def get(self):
        global offset_location
        print("Incoming from FE")
        if delivery_status =="IDLE":
            to_send=dict()
            sent=data['list'][1:]
            to_send['list']=sent
            offset_location=0.0
            return jsonify(to_send)
        else:
            data["list"][0]['Pos'] = [position_lat-offset_location*0.3, position_lon+offset_location]
            offset_location+=0.01
            data["list"][0]['CurrVolume'] = volume
            if vibration or lock_status or theft :
                warning = "theft"
            else:
                warning = "none"
            data["list"][0]['Warning'] = warning
            return jsonify(data)

class DeviceEndPoint(Resource):
    def post(self):
        global lock_command
        global volume
        global vibration
        global lock_status
        global position_lat
        global position_lon
        global theft
        json_data = request.get_json(force=True)
        print("Incoming from device")
        print(json_data)
        
        position_lat = -6.182411# json_data['lat']
        position_lon = 106.824540#json_data['lon']
        lock_status = json_data['lock']
        vibration = json_data['vibration']
        if vibration == True:
          theft = True
        volume = json_data['vol'] * 10.0
        if delivery_status == "SENDING":
            lock_command = True
            print("lock true")
        else:
            lock_command = False
        print(lock_command)
        return jsonify({"lock":lock_command})

class MobileEndPoint(Resource):
    def get(self):
        return {'hello': 'world'}
    def post(self):
        global delivery_status, theft
        json_data = request.get_json(force=True)
        print("incoming from mobile")
        print(json_data)
        if json_data['action'] == "RECEIVE" :
            # receive
            delivery_status = "IDLE"
            theft = False
        else:
            # send
            delivery_status = "SENDING"
        print(delivery_status)
        return jsonify({"status": delivery_status})

api.add_resource(MobileEndPoint, '/mobile')
api.add_resource(DeviceEndPoint, '/device')
api.add_resource(FrontEndpoint, '/front')
if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)

