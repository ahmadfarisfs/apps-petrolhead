import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';
import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MaterialApp(home: LoginPage()));

const flash_on = "FLASH ON";
const flash_off = "FLASH OFF";
const front_camera = "FRONT CAMERA";
const back_camera = "BACK CAMERA";
const TextStyle myStyle = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

class Post {
  final String driverId;
  final String routeId;
  final String truckId;
  final String action;
  Post({this.driverId, this.routeId, this.truckId, this.action});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["driverId"] = driverId;
    map["routeId"] = routeId;
    map["truckId"] = truckId;
    map["action"] = action;
    return map;
  }
}

Future<bool> createPost(String url, Map jsonMap) async {
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('content-type', 'application/json');
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();
  // todo - you should check the response.statusCode
  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  return true;

/*  return http.post(url, body: body, headers: {"Content-Type": "application/json"}).then((http.Response response) {
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return true;
  });*/
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
      obscureText: false,
      style: myStyle,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final passwordField = TextField(
      obscureText: true,
      style: myStyle,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MenuPage()),
          );
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: myStyle.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 155.0,
                  child: Image.asset(
                    "assets/logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: 45.0),
                emailField,
                SizedBox(height: 25.0),
                passwordField,
                SizedBox(
                  height: 35.0,
                ),
                loginButon,
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class OKSendPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment:  CrossAxisAlignment.center,
          children: <Widget>[
          Padding( padding: EdgeInsets.all(20), child: Text("Delivery in progress to SPBU Coco 90054", style: TextStyle(fontSize: 18),)),
RawMaterialButton(
  onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MenuPage()),
                );
              },
  child: new Icon(
     Icons.home,
     color: Colors.blue,
     size: 35.0,
  ),
  shape: new CircleBorder(),
  elevation: 2.0,
  fillColor: Colors.white,
  padding: const EdgeInsets.all(15.0),
),

          ],
        ),
      ),
    );
  }
}

class TripSummaryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment:  CrossAxisAlignment.center,
          children: <Widget>[
          Padding( padding: EdgeInsets.all(20), child: Text("Delivery Success to SPBU Coco 90054", style: TextStyle(fontSize: 18),)),
          Padding( padding: EdgeInsets.all(10), child: Text("Late by 20 minutes", style: TextStyle(fontSize: 15),)),
                    Padding( padding: EdgeInsets.all(10), child: Text("No theft and warning on the way", style: TextStyle(fontSize: 15),)),

RawMaterialButton(
  onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MenuPage()),
                );
              },
  child: new Icon(
     Icons.check,
     color: Colors.blue,
     size: 35.0,
  ),
  shape: new CircleBorder(),
  elevation: 2.0,
  fillColor: Colors.white,
  padding: const EdgeInsets.all(15.0),
),

          ],
        ),
      ),
    );
  }
}

class MenuPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final receiveButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.orangeAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => QRScanPage(action: "Scan Driver ID.")),
          );
        },
        child: Text("Receive",
            textAlign: TextAlign.center,
            style: myStyle.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
    final sendButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SelectTripPage()),
          );
        },
        child: Text("Send",
            textAlign: TextAlign.center,
            style: myStyle.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        body: Center(
            child: Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(36.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(children: <Widget>[
              SizedBox(
                height: 155.0,
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Image.asset(
                      "assets/profile.jpg",
                      fit: BoxFit.contain,
                    )),
              ),
              Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text('John Smith',
                          style: myStyle.copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 25)),
                      Text("Surveyor"),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Text("4.0")
                        ],
                      )
                    ],
                  ))
            ]),
            SizedBox(height: 45.0),
            sendButton,
            SizedBox(height: 25.0),
            receiveButton,
          ],
        ),
      ),
    )));
  }
}

class QRScanPage extends StatefulWidget {
  const QRScanPage({Key key, String this.action}) : super(key: key);
  final String action;
  @override
  State<StatefulWidget> createState() => _QRScanPageState(this.action);
}

class _QRScanPageState extends State<QRScanPage> {
  static final CREATE_POST_URL = 'http://10.50.0.53:5000/mobile';

  void _pushData(bool isReceive) async {
    print("Pushing data");
    String action;
    if (isReceive){
      action = "RECEIVE";
    }else{
      action = "SEND";
    }
    print(action);
    Post newPost = new Post(driverId: "123", truckId: "0", routeId: "0", action: action);
    bool p = await createPost(CREATE_POST_URL,  newPost.toMap());
    print("wait done");
  }

  var qrText = "";
  var flashState = flash_on;
  var cameraState = front_camera;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  String action;
  _QRScanPageState(this.action);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Text(this.action),
            Expanded(
              child: QRView(
                key: qrKey,
                onQRViewCreated: _onQRViewCreated,
                overlay: QrScannerOverlayShape(
                  borderColor: Colors.red,
                  borderRadius: 10,
                  borderLength: 30,
                  borderWidth: 10,
                  cutOutSize: 300,
                ),
              ),
              flex: 4,
            ),
            Text("QR: " + qrText, style: myStyle),
            RaisedButton(
              onPressed: () {
                if (this.action == "Scan Driver ID") {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            QRScanPage(action: "Scan Device ID")),
                  );
                } else if (this.action == "Scan Device ID") {
                  _pushData(false);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OKSendPage(),
                      ));
                } else if (this.action == "Scan Driver ID.") {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            QRScanPage(action: "Scan Device ID.")),
                  );
                } else if (this.action == "Scan Device ID.") {
                  _pushData(true);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TripSummaryPage(),
                      ));
                }
              },
              child: Text("Validate"),
            ),
          ],
        ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen(
      (scanData) {
        setState(
          () {
            qrText = scanData;
          },
        );
      },
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class SelectTripPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Select delivery';

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: ListTile(
              leading: Icon(
                Icons.airport_shuttle,
                color: Colors.green,
                size: 60,
              ),
              title: Text('SPBU Coco 90054'),
              subtitle: Text('Premium 16000 Liter'),
              trailing: Icon(
                Icons.timer,
                color: Colors.blue,
              ),
              isThreeLine: false,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => QRScanPage(
                            action: "Scan Driver ID",
                          )),
                );
              },
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.airport_shuttle,
                color: Colors.green,
                size: 60,
              ),
              title: Text('SPBU 7754'),
              subtitle: Text('Pertalite 8000 Liter'),
              trailing: Icon(
                Icons.timer,
                color: Colors.blue,
              ),
              isThreeLine: false,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => QRScanPage(
                            action: "Scan Driver ID",
                          )),
                );
              },
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.airport_shuttle,
                color: Colors.red,
                size: 60,
              ),
              title: Text('[LATE] SPBU Coco 95054'),
              subtitle: Text('Pertamax 16000 Liter'),
              trailing: Icon(
                Icons.timer,
                color: Colors.red,
              ),
              isThreeLine: false,
            ),
          ),
        ],
      ),
    );
  }
}
